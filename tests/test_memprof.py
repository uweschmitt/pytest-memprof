# -*- coding: utf-8 -*-

import csv
import os


def _makepyfile(testdir):
    # create a temporary pytest test module
    testdir.makepyfile(
        """
        def test_memory_consumption():
            import time
            time.sleep(.5)
            data = list(range(100000))
            time.sleep(1.0)
            len(data)
            del data

        def test_memory_consumption_2():
            import time
            time.sleep(.5)
            data = list(range(1000000))
            time.sleep(1.0)
            len(data)
            del data
    """
    )


def test_memory_profiling(testdir):
    """Make sure that pytest accepts our fixture."""

    _makepyfile(testdir)

    # run pytest with the following cmd args
    # fnmatch_lines does an assertion internally
    result = testdir.runpytest("--memprof-top-n", "0")
    result.stdout.fnmatch_lines(
        [
            "test_memory_profiling.py::test_memory_consumption_2  - ** MB",
            "test_memory_profiling.py::test_memory_consumption    - ** MB",
        ]
    )

    # make sure that that we get a '0' exit code for the testsuite
    assert result.ret == 0

    # run pytest with the following cmd args
    result = testdir.runpytest("--memprof-top-n", "1")

    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        ["test_memory_profiling.py::test_memory_consumption_2  - ** MB"]
    )


def test_help_message(testdir):
    result = testdir.runpytest("--help")
    # fnmatch_lines does an assertion internally
    result.stdout.fnmatch_lines(
        [
            "memprof:",
            "*--memprof-top-n=MEMPROF_TOP_N*",
            "*limit memory reports to top n entries, report all*",
        ]
    )


def test_csv_out(testdir):
    """Make sure that pytest accepts our fixture."""

    _makepyfile(testdir)
    csvname = testdir.tmpdir.join("memprof.csv").strpath

    # run pytest with the following cmd args
    _ = testdir.runpytest("--memprof-csv-file", csvname)
    reader = csv.reader(open(csvname, 'rt', newline=''))
    header = next(reader)
    assert header == ['function', 'memory']
    rows = [r for r in reader]
    assert len(rows) >= 2
    # limit reported items to 1
    result = testdir.runpytest("--memprof-top-n", "1", "--memprof-csv-file", csvname)
    reader = csv.reader(open(csvname, 'rt', newline=''))
    header = next(reader)
    assert header == ['function', 'memory']
    rows = [r for r in reader]
    assert len(rows) == 1
